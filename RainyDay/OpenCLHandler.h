#ifndef OPENCL_HANDLER_H
#define OPENCL_HANDLER_H

#include "stafx.h"
#include <Windows.h>
#include <map>
#include <string>

class OpenCLHandler {

public:

	OpenCLHandler(const char* fname);
	~OpenCLHandler();

	bool InitKernel( const char* fname );

	void ReadBuffer();
	void PushData();

	cl_context GetContext()			{	return this->context;	}
	cl_command_queue	GetQueue()	{	return queue;			}

	cl_kernel& GetKernel(const char* fname)			{	return kernels[fname];	}

	size_t		workGroup;

	void PrintCLError(cl_int err, std::string message);
	const char*	clErrorString(cl_int err);

private:
	
	//to store each function
	std::map<const char*, cl_kernel>				kernels;
	std::map<const char*, cl_kernel>::iterator		kernelItr;

	void PrintBuildLog(cl_int iBuild, cl_program program);

	GLuint iDeviceCount, iPlatformCount;

	//handles to for many devices and platforms
	cl_device_id*			devices;
	cl_platform_id*			platforms;

	//key cl apparatus
	cl_context				context;
	cl_command_queue		queue;
	cl_program				program;

	cl_int					err;
	cl_int					iBuildProgram;
	cl_int					platformHandle;
	cl_event				clEvent;

	bool LoadProgram(const char* fname);
	char* LoadFile(const char* fname);
	bool BuildContext();
	bool CheckPlatform();
	bool GetDevice();
};

#endif