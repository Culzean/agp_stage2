#include "Object.h"

Object::Object() {
	this->iBufferCount = 4;
	this->vbo = new GLuint[iBufferCount];
}

Object::~Object() {

    delete [] pTexCoords;

	glDeleteBuffers(this->iBufferCount, vbo);
	glDeleteVertexArrays(1, &vao);
	delete [] vbo;
	delete [] pIndexes;
	delete [] pVerts;
	delete [] pNorms;

	/*if(this->debug)
		delete debug;*/
}

bool Object::loadData() {
	//current loading from .h

	/*this->iNumFaces = numGourdFaces;
	this->iNumVerts = numGourdVerts;

	pVerts = new GLfloat[numGourdVerts * 3 * 3];
	pNorms = new GLfloat[numGourdVerts * 3 * 3];
	pIndexes = new GLuint[numGourdFaces * 3 * 3];
	pTexCoords = new GLfloat[numGourdFaces * 3 * 3];
	int j = 0;
	for(int i=0; i < numGourdVerts; i++)
	{
		pVerts[j] = GourdVerts[i][0];
		pVerts[j+1] = GourdVerts[i][1];
		pVerts[j+2] = GourdVerts[i][2];

		pNorms[j] = GourdVertNorms[i][0];
		pNorms[j+1] = GourdVertNorms[i][1];
		pNorms[j+2] = GourdVertNorms[i][2];
		j+=3;
	}
	j =0;
	for(int i=0; i < numGourdFaces; ++i)
	{
		pIndexes[j] = GourdFaces[i][0];
		pIndexes[j+1] = GourdFaces[i][1];
		pIndexes[j+2] = GourdFaces[i][2];
		j+=3;
	}
	EndMesh();*/

	return true;
}

void Object::AddDebug(AGPShader* shaderManager) {

	debug = new DebugRend();

	debug->setShaderID( shaderManager->initShaderPair("debug.vert", "debug.frag", AGP_SHADER_DEBUG) );
	debug->setShaderType( AGP_SHADER_DEBUG );
	debug->loadData();

	debug->loadNorms(pNorms, iNumVerts);

}

void Object::AddTriangle(glm::vec3 verts, glm::vec3 norms, glm::vec2 texCoords) {



}

void Object::BeginMesh(GLuint nNumVerts) {

	

}

void Object::EndMesh() {

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(this->iBufferCount, vbo);

	//bind first vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, iNumVerts * sizeof(GLfloat) * 3, pVerts , GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	//bind normals buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, iNumVerts * sizeof(GLfloat) * 3, pNorms , GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(2);

	//bind second vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]); // bind 2nd VBO as active buf obj 
	// Copy the colour data from colours to our buffer
	// 15 * sizeof(GLfloat) is the size of the colours array
	glBufferData(GL_ARRAY_BUFFER, iNumVerts * sizeof(GLfloat) * 3, pVerts, GL_STATIC_DRAW);
	// Colour data is going into attribute index 1 & has 3 floats per vertex 
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);    // Enable attribute index 1 (color)

	//indices to be bound next!
	//note the GL_ELEMENT_ARRAY_BUFFER is not GL_ARRAY_BUFFER
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iNumFaces * sizeof(GLuint) * 3, pIndexes,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void Object::Draw(AGPShader* shader) {

	//so draw already
	glBindVertexArray(this->vao);
	//move this to the object class
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[3]);
	glDrawElements(GL_TRIANGLES, numGourdFaces * 3, GL_UNSIGNED_INT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//make bool check!
	if(debug)
		{
			shader->useShader( debug );
			debug->Draw(shader);
		}

	shader->onFinish(this->getShaderType());
}

void Object::Draw(AGPShader* shader, glm::vec3 input) {

}