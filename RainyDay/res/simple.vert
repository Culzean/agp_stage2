// simple vertex shader - simple.vert
#version 130
// OpenGL 3.2 replace above with #version 150

uniform mat4x4 MV;
uniform mat4x4 projection;
in  vec3 in_Position;
in  vec3 in_Color;
out vec3 ex_Color;

// simple shader program
// multiply each vertex position by the MVP matrix
void main(void) {
    vec4 vertexPosition = MV * vec4(in_Position,1.0);
	gl_Position = projection * vertexPosition;
    ex_Color = in_Color; // pass colour unmodified
}
