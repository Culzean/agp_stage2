#include "GameData.h"

//fast data declarations
GLfloat materialHard[][4] = {
	{ 0.2, 0.2, 0.2, 1.0 },
	{ 0.8, 0.2, 0.2, 1.0 },
	{ 0.2, 0.4, 0.3, 1.0 },
	{1.0f},
	};

GLfloat lightHard[][4] = {
	{ 1.0f, 0.2f, 0.2f, 1.0f },
	{ 0.8, 0.8, 0.8, 1.0 },
	{ 1.0, 1.0, 1.0, 1.0 },
	{ 0.0, 3.0, 5.0, 1.0 },
};

GameData::GameData() {

}

GameData::~GameData() {

}

void GameData::Init() {

	//init game resources here
	texManager = TextureManager::GetInstance();

	player.Init( glm::vec3(0.0f, -1.0f, -8.0f),glm::vec3(0.0f), glm::ivec2(SCREEN_WIDTH, SCREEN_HEIGHT) );

	glm::vec3 pos0(-10.0, 1.0, 10.0);
	glm::vec3 pos1(-10.0, 1.0, -10.0);
	glm::vec3 pos2(10.0, 1.0, -10.0);
	glm::vec3 pos3(10.0, 1.0, 10.0);

	light0 = new light();

	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
	glEnable(GL_BLEND);
	this->shader.SetCamera(&this->player);

	glPointSize(10);
	Plane* pPlane = new Plane(pos0,pos1,pos2,pos3);
	pPlane->Init(2,10);
	pPlane->getMesh()->setShaderID(shader.initShaderPair("res/simple.vert", "res/simple.frag", AGP_SHADER_SIMPLE));
	pPlane->getMesh()->setShaderType(AGP_SHADER_SIMPLE);

	loadMaterial(light0);

	drawList.push_back(pPlane->getMesh());

	flock0 = new Flock(pPlane->getWidth(), pPlane->getDepth());
	flock0->setShaderID(shader.initShaderPair("res/particle.vert", "res/particle.frag", AGP_SHADER_PARTICLE));
	flock0->setTexID( this->texManager->loadTexture("res/Fish.bmp") );
	flock0->setShaderType(AGP_SHADER_PARTICLE);

	Boid pTemp;
	for(int i =0 ; i< 35; ++i)
	{
		//there is no shader look up!! shoddy
		//create many boids
		glm::vec4 valPos( glm::vec4( glm::compRand1(0, pPlane->getWidth()), 5, glm::compRand1(0, pPlane->getDepth()), 0.0 ) );
		glm::vec4 valVel( 0.0f );
		glm::vec4 valAccel( 0.0f );

		pTemp = Boid(valPos, valVel, valAccel);
		pTemp.iBoidSeen = 0;
		flock0->AddBoid(pTemp);
		drawList.push_back(flock0);
		//drawList.push_back(pTemp);
	}
	flock0->Bind();
}

void GameData::loadMaterial( light* light)
{
	int i = 0;
	glm::vec4 tempVec;
	for(i = 0; i < 4 ; ++i)
	{
		tempVec[i] =  materialHard[0][i];
		light->ambient[i] = lightHard[0][i];
	}
	//mat->setAmbient( tempVec );
	for(i = 0; i < 4 ; ++i)
	{
		tempVec[i] = materialHard[1][i];
		light->diffuse[i] = lightHard[1][i];
	}
	//mat->setDiffuse(tempVec);
	for(i = 0; i < 4 ; ++i)
	{
		tempVec[i] = materialHard[2][i];
		light->specular[i] = lightHard[2][i];
	}
	//mat->setSpecular(tempVec);
	//mat->setShininess(materialHard[3][0]);
	for(i = 0; i < 4 ; ++i)
	{
		light->position[i] = lightHard[3][i];
	}
}

void GameData::CleanUp() {

	//delete game assets here!
	for(int i =0; i< drawList.size(); ++i)
	{
		delete drawList[i];
	}
}