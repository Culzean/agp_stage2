#include "Flock.h"


//some const values required by the boids
const static GLuint		MAX_BOIDS =		256;

const static GLfloat	MAX_SPEED =		0.01f;
const static GLfloat	MIN_URGENCY =	0.0005f;
const static GLfloat	MAX_URGENCY =	0.0001f;
const static GLfloat	MAX_CHANGE =	MAX_SPEED *	MAX_URGENCY;
const static GLfloat	MAX_VIEW =		5; // this is currently repeated value in the kernel



Flock::Flock(GLint _width, GLint _depth) {
	clHandler = new OpenCLHandler("res/boidKernel.cl");
	clHandler->InitKernel("boids");
	//clHandler->InitKernel("test");
	averageSpeed = MAX_SPEED / 2;
	iNoBoids = 0;

	flock.resize(MAX_BOIDS);
	Init(_width, _depth);
	clBounds.x = _width;
	clBounds.y = _depth;
	clBounds.z = _depth;
}

Flock::~Flock() {
	clReleaseMemObject(this->clPos);
	clReleaseMemObject(this->clAccel);
	clReleaseMemObject(this->clVel);
	clReleaseMemObject(this->clFlockIn);
	clReleaseMemObject(this->clRand);


	glDeleteBuffers(iNoBuffers, vbo);
	glDeleteVertexArrays(1, &vao);
	delete vbo;

	delete clHandler;
	for(int i=0; i<MAX_BOIDS;	++i) {
		delete flock[i];
	}

	delete [] vVel;
	delete [] vAccel;
	delete [] vPos;
}

void Flock::Init() {
	//random values
	Init(20, 20);
}

void Flock::Init(GLuint _width, GLuint _depth) {

	this->iNoBuffers = 4;
	vbo = new GLuint[iNoBuffers];

	this->vVel = new glm::gtx::cl_float4[MAX_BOIDS];
	this->vAccel = new glm::gtx::cl_float4[MAX_BOIDS];
	this->vPos = new glm::gtx::cl_float4[MAX_BOIDS];

	this->setMaxSpeed(MAX_SPEED);

	for(int i =0; i< MAX_BOIDS; ++i)
	{
		this->flock[i] = new Boid();

		flock[i]->vVel = glm::vec4(0.0f);
		flock[i]->vAccel = glm::vec4(0.0f);
		flock[i]->vPos = glm::vec4(glm::compRand1(0.0f, _width), 3, glm::compRand1(0.0f,_depth), 0.0f);
	}
	DefineInputs();
}

void Flock::Bind() {

	//created all client data
	//now create buffers
	this->BindGLBuffers();
	this->BindCLBuffers();
	this->BindKernel();
}

bool Flock::AddBoid(Boid _newBoid) {

	//assign a vel and accel value
	//give an id and add to vector(maybe a map)
	//Will then need to rebind the gl buffer

	if( ++iNoBoids > MAX_BOIDS)
	{
		--iNoBoids;
		std::cout << "no more space at the inn" << std::endl;
		return false;
	}
	//flock[iNoBoids] = new 
	flock[iNoBoids]->vPos = _newBoid.vPos;
	flock[iNoBoids]->vVel = _newBoid.vVel;
	flock[iNoBoids]->vAccel = _newBoid.vAccel;
	return true;
}

void Flock::Update() {

	//ok for the mean time use a for loop for all the boids
	//this contray to the idea of using a gpu
	//but need some updated random
	//also need to retrieve some data for display
	//retrieve velocity and position

	//create some random
	glm::gtx::cl_float4* random = new glm::gtx::cl_float4[iNoBoids];
	for(int i =0 ; i<iNoBoids; ++i)
	{
		glm::vec4 temp = glm::compRand4(0.0f, 1.0f);
		random[i] = temp;
	}
	//wait for gl
	glFinish();
	size_t size = iNoBoids * sizeof(glm::gtx::cl_float4);

	cl_int status;
	cl_event wait;

	//feed in data
	status = clEnqueueWriteBuffer( clHandler->GetQueue(), clRand, CL_TRUE, 0 ,size, random, 0, NULL, NULL );
	if(status != CL_SUCCESS)
		clHandler->PrintCLError(status, "clEnqueue random write error : ");

	clEnqueueWriteBuffer( clHandler->GetQueue(), clPos, CL_TRUE, 0 ,sizeof(glm::gtx::cl_float4), &vUserInput, 0, NULL, NULL );
	clSetKernelArg(clHandler->GetKernel("boids"), 1, sizeof(glm::gtx::cl_float4), (void *) &this->vUserInput);

	/*	status = clEnqueueWriteBuffer( clHandler->GetQueue(), clFlockIn, CL_TRUE, 0 , sizeof(Boid) * iNoBoids, &this->flock[0], 0, NULL, &wait );
	status = clWaitForEvents(1, &wait);
		if(status != CL_SUCCESS) {
      std::cout << "error creating buffer object, clVel  " << clHandler->clErrorString(status) << std::endl;
	}*/

	status = clEnqueueAcquireGLObjects(	clHandler->GetQueue(), 1, &clPos, 0, NULL, NULL	);
	if(status != CL_SUCCESS)
		clHandler->PrintCLError(status, "clEnqueue GL object Error: ");

	
	size_t globalWorkSize = iNoBoids;
	//currently letting the API set alot of default values, some efficiency gains here
	status = clEnqueueNDRangeKernel( clHandler->GetQueue(), clHandler->GetKernel("boids"), 1, NULL, &globalWorkSize, NULL, 0, NULL, &wait );
	if(status != CL_SUCCESS)
		clHandler->PrintCLError(status, "clEnqueue ND Range Error: ");
	//collect results
	status = clWaitForEvents(1, &wait);
	clEnqueueReadBuffer( clHandler->GetQueue(), clPos, CL_FALSE, 0, size, vPos, 0 , NULL, &wait );
	if(status != CL_SUCCESS)
		clHandler->PrintCLError(status, "read vPos error : ");

	/*status = clEnqueueReadBuffer( clHandler->GetQueue(), clFlockIn, CL_TRUE, 0, sizeof(Boid) * iNoBoids, &this->flock[0], 0 , NULL, &wait );
	if(status != CL_SUCCESS)
		clHandler->PrintCLError(status, "cl read buffer error : ");*/

	status = clWaitForEvents(1, &wait);
	if(status != CL_SUCCESS)
		clHandler->PrintCLError(status, "cl wait for events error: ");

	//wait for cl
	clFinish( clHandler->GetQueue() );
	clEnqueueReleaseGLObjects( clHandler->GetQueue(), 1, &this->clPos, 0, NULL, NULL );

	delete [] random;
}


void Flock::Draw(AGPShader* shader) {

	shader->useShader(this);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, this->iNoBoids * sizeof(glm::gtx::cl_float4), vPos, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(ATTRIBUTE_VERTEX, 4, GL_FLOAT, GL_FALSE, 0, 0 );
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 4, GL_FLOAT, GL_FALSE, 0, 0 );
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);

	glDrawArrays(	GL_POINTS, 0, iNoBoids	);

	glDisableVertexAttribArray(ATTRIBUTE_COLOR);
	glDisableVertexAttribArray(ATTRIBUTE_VERTEX);

	shader->onFinish(getShaderType());

	/*for(int i=0; i< iNoBoids; ++i)
	{
		glm::vec4 vDraw(flock[i]->vVel);
		vDraw *= 50;
		vDraw += vPos[i];
		Flock::DrawDebug( vDraw, vPos[i] );
	}*/
}

void Flock::BindGLBuffers() {

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(this->iNoBuffers, vbo);

	//and sort the particle system
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_VERTEX]); // bind VBO for positions
	glBufferData(GL_ARRAY_BUFFER, this->iNoBoids * sizeof(glm::vec4), this->vPos, GL_DYNAMIC_DRAW); //DYNAMIC
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);     // Enable attribute index 0

	// Colours data in attribute 1, 3 floats per vertex
	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_COLOR]); // bind VBO for colours
	glBufferData(GL_ARRAY_BUFFER, this->iNoBoids * sizeof(glm::vec4) , vPos, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);    // Enable attribute index 1

	glEnable(GL_DEPTH_TEST);
}

void Flock::BindKernel( ) {

	//buffer object associated with the particle system that uses them
	//so bind the kernel arguments here, which will change depending on particle array type

	cl_int status;
	size_t size = sizeof(cl_mem); 


	status  = clSetKernelArg(clHandler->GetKernel("boids"), 0, size, &clFlockIn);
	status |= clSetKernelArg(clHandler->GetKernel("boids"), 1, sizeof(glm::gtx::cl_float4), (void *) &this->vUserInput);
	status |= clSetKernelArg(clHandler->GetKernel("boids"), 2, size, &clRand);
	status |= clSetKernelArg(clHandler->GetKernel("boids"), 3, size, &this->clPos);
	status |= clSetKernelArg(clHandler->GetKernel("boids"), 4, sizeof(GLuint), &this->iNoBoids);
	status |= clSetKernelArg(clHandler->GetKernel("boids"), 5, sizeof(glm::gtx::cl_float4), (void *) &this->clBounds);
	status |= clSetKernelArg(clHandler->GetKernel("boids"), 6, sizeof(cl_float), (void *)  &this->clMaxSpeed);
	if(status != CL_SUCCESS) {
		clHandler->PrintCLError(status, "arguement binding failed  " );
   }else
	std::cout << "particle array arguments loaded and ready " << std::endl;
}

bool Flock::BindCLBuffers() {

	size_t size = sizeof(Boid) * iNoBoids; 
	cl_int err;

	clPos = clCreateFromGLBuffer( clHandler->GetContext(), CL_MEM_READ_WRITE, vbo[ATTRIBUTE_VERTEX], &err );
	if(err != CL_SUCCESS || clPos == NULL) {
      std::cout << "error creating buffer object, clPos  " << clHandler->clErrorString(err) << std::endl;
	  return false;
	}
	//create context. parameters here set what actions and data flow direction of buffer


	clFlockIn = clCreateBuffer(clHandler->GetContext(), CL_MEM_READ_WRITE, size, NULL , &err);
	if(err != CL_SUCCESS || clVel == NULL) {
      std::cout << "error creating buffer object, clFlockIn  " << clHandler->clErrorString(err) << std::endl;
	  return false;
	}

	err = clEnqueueWriteBuffer( clHandler->GetQueue(), clFlockIn, CL_FALSE, 0 , size, &this->flock[0], 0, NULL, NULL );
	if(err != CL_SUCCESS) {
      std::cout << "error creating buffer object, clFlockIn  " << clHandler->clErrorString(err) << std::endl;
	  return false;
	}

	clFlockOut = clCreateBuffer(clHandler->GetContext(), CL_MEM_READ_WRITE, size, NULL , &err);
	if(err != CL_SUCCESS || clVel == NULL) {
      std::cout << "error creating buffer object, clVel  " << clHandler->clErrorString(err) << std::endl;
	  return false;
	}

	err = clEnqueueWriteBuffer( clHandler->GetQueue(), clFlockOut, CL_FALSE, 0 , size, &flock[0], 0, NULL, NULL );
	if(err != CL_SUCCESS) {
      std::cout << "error creating buffer object, clVel  " << clHandler->clErrorString(err) << std::endl;
	  return false;
	}

	clRand = clCreateBuffer( clHandler->GetContext(), CL_MEM_ALLOC_HOST_PTR, size, NULL, &err );
	if(err != CL_SUCCESS)
		clHandler->PrintCLError(err, "clBuffer error : ");

	std::cout << "buffers created and ready " << std::endl;
	return true;
}

void Flock::SetInput( GLint value, GLint incr ) {

	if(incr > 0)
	{
		this->vUserInput[value] += vUserSteps[value];
		if(vUserInput[value] > vUserMax[value])
		{ vUserInput[value] = vUserMax[value]; }
	}
	if(incr < 0)
	{
		this->vUserInput[value] -= vUserSteps[value];
		if(vUserInput[value] < vUserMin[value])
		{ vUserInput[value] = vUserMin[value]; }
	}
	std::cout << "User input: " << value << "  " << vUserInput[value] << std::endl;
}

void Flock::DefineInputs() {

	vUserSteps[0] = static_cast< GLint >( (MAX_BOID_VIEW - MIN_BOID_VIEW) / NO_INCR );
	vUserSteps[1] = ( (MAX_VIEW_DIST - MIN_VIEW_DIST) / NO_INCR );
	vUserSteps[2] = ( (MAX_FLOCK_COEH - MIN_FLOCK_COEH) / NO_INCR );
	vUserSteps[3] = ( (MAX_ALIGN - MIN_ALIGN) / NO_INCR );

	vUserMax[0] = MAX_BOID_VIEW;
	vUserMax[1] = MAX_VIEW_DIST;
	vUserMax[2] = MAX_FLOCK_COEH;
	vUserMax[3] = MAX_ALIGN;

	vUserMin[0] = MIN_BOID_VIEW;
	vUserMin[1] = MIN_VIEW_DIST;
	vUserMin[2] = MIN_FLOCK_COEH;
	vUserMin[3] = MIN_ALIGN;

	vUserInput[0] = MIN_BOID_VIEW + vUserSteps[0] * 2;
	vUserInput[1] = MIN_VIEW_DIST + vUserSteps[1] * 2;
	vUserInput[2] = MIN_FLOCK_COEH + vUserSteps[2] * 2;
	vUserInput[3] = MIN_ALIGN + vUserSteps[3] * 2;
}


void Flock::DrawDebug(glm::vec4 _vDraw, glm::vec4 _vStart) {

	//glFlush();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindBuffer(GL_ARRAY_BUFFER, 1);

	glBindVertexArray(0);

	glm::vec4 data[] = { _vStart, _vDraw };

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glm::vec4 col(0.0f,1.0f,0.0f,1.0f);
	glColorPointer(4, GL_FLOAT, 0 , &col);
	glVertexPointer(4, GL_FLOAT, 0, data);
	glDrawArrays(GL_LINES, 0, 2 );

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}