/*#include "Boid.h"

Boid::Boid() {

}

Boid::~Boid() {
	glDeleteBuffers(iNoBuffers, vbo);
	glDeleteVertexArrays(1, &vao);

	delete [] vbo;

}

void Boid::Init() {
	this->Init( glm::vec3( glm::compRand1(0, 20), 5, glm::compRand1(0, 20) ), false );
	
}

void Boid::Init(glm::vec3 _pos, bool drawDebug) {

	vPos = _pos;
	vVel = glm::vec3(0.0f);
	vOrient = glm::vec3(0.7f, -0.9f, 0.3f);
	vAngle = glm::vec3(0.0);

	iNoBuffers = 4;

	vbo = new GLuint[iNoBuffers];

	this->debug = drawDebug;
	BindBuffers();

}

void Boid::BindBuffers() {

	glBindTexture(0, this->texID);

	glGenVertexArrays(1,&this->vao);
	glBindVertexArray(vao);

	glGenBuffers(iNoBuffers, this->vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // bind VBO for positions
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3), &vPos, GL_DYNAMIC_DRAW); //DYNAMIC
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);     // Enable attribute index 0

	// Colours data in attribute 1, 3 floats per vertex
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // bind VBO for colours
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3), &vPos, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);    // Enable attribute index 1

	glEnable(GL_DEPTH_TEST);
}


void Boid::Draw(AGPShader* shader) {
	
	shader->useShader(this);
	
	glBindVertexArray(this->vao); // bind member vao

	//handle texture
	GLuint ShaderId;
		GLuint texUniform = glGetUniformLocation(shaderID, "texture0");
		glUniform1i( texUniform, 0 );

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->getTexID());

		// Now draw the particles... as easy as this!
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); //RUGH?
		glDepthMask(0);
		glEnable(GL_BLEND);

	// particle data updated - so need to resend to GPU
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // bind VBO
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3), &vPos, GL_DYNAMIC_DRAW);
	// Position data in attribute index 0, 3 floats per vertex
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);     // Enable attribute index 0
	
	glDrawArrays(GL_POINTS, 0, 1 );

	shader->onFinish(this->getShaderType());

	glBindVertexArray(0);

	if(this->debug)
	{
		shader->useDebugShader();
		//DrawDebug(vOrient+glm::vec3(vPos), this->vPos);
	}
}

void Boid::Update() {


}

void Boid::setPos(glm::vec4 _pos) {
	vPrevPos = vPos;
	vPos.x = _pos.x;	vPos.y = _pos.y;	vPos.z = _pos.z;
}

void Boid::setVel(glm::vec4 _vel) {
	vPrevVel = vVel;
	vVel.x = _vel.x;	vVel.y = _vel.y;	vVel.z = _vel.z;
}

void Boid::setOrient(glm::vec4 _ornt) {
	this->vOrient.x = _ornt.x;	vOrient.y = _ornt.y;	vOrient.z = _ornt.z;
}


bool Boid::InterpolateHeading() {

	glm::vec3 vChange(vVel - vPrevVel);

	GLfloat roll, pitch, yaw;

	glm::vec3 lateralDir = glm::cross( glm::cross(this->vVel, vChange ), vVel );

	glm::normalize(lateralDir);

	glm::vec3 temp = lateralDir;
	temp *= (vVel- vPrevVel);

	GLfloat lateralMag = temp.length();


	if(lateralMag == 0) {
		roll = 0.0f;
	}else {
		roll = -glm::atan(GRAVITY, lateralMag) + HALF_PI;
	}

	pitch = -glm::atan(vVel.y / glm::sqrt( (vVel.z * vVel.z) + (vVel.x * vVel.x) ) );

	yaw = glm::atan(vVel.x, vVel.z);

	vAngle.x = pitch;
	vAngle.y = yaw;
	vAngle.z = roll;

	//convert into a direction vector
	vOrient.x = glm::cos(roll)*glm::cos(pitch);
	vOrient.y = glm::sin(roll)*glm::cos(pitch);
	vOrient.z = glm::sin(pitch);

	glm::normalize(vOrient);
	vOrient *= -1.2;

	//std::cout << "orientation: " << vOrient.x << "   " << vOrient.y << "   "<< vOrient.z << std::endl;

	return true;
}

void Boid::DrawDebug(glm::vec3 _vDraw, glm::vec3 _vStart) {

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindBuffer(GL_ARRAY_BUFFER, 1);

	glBindVertexArray(0);

	glm::vec3 data[] = { _vStart, _vDraw };

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glm::vec4 col(0.0f,1.0f,0.0f,1.0f);
	glColorPointer(4, GL_FLOAT, 0 , &col);
	glVertexPointer(3, GL_FLOAT, 0, data);
	glDrawArrays(GL_LINES, 0, 2 );

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}*/