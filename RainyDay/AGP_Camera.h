#ifndef AGP_CAMERA_H
#define AGP_CAMERA_H

#include "stafx.h"

class AGP_Camera {

public:
	AGP_Camera();
	~AGP_Camera()		{};

	void Init(glm::vec3 _startPos, glm::vec3 _startLook, glm::ivec2 _screenDim);

	void Update(glm::ivec2	_mousePos);
	bool InputUp(SDL_Event const &sdlEvent);
	bool InputDown(SDL_Event const &sdlEvent);

	glm::mat4 getCamera()			{	return cameraMatrix;	};
	glm::vec3 getPos()				{	return this->pos;	};


private:

	glm::mat4 cameraMatrix;

	GLfloat		horAngle;
	GLfloat		vertAngle;
	
	glm::vec3	vUp;
	glm::vec3	vForward;
	glm::vec3	vRight;

	glm::vec3	pos;
	glm::vec3	vStrafe;
	glm::vec2	vStrafeState;

	glm::ivec2	screenDim;
	GLfloat		speed;
	GLfloat		mouseSpeed;

};

#endif