#include "AGP_Camera.h"

const static GLfloat PLAYER_MOVE_MED = 5.4f;
const static GLfloat PLAYER_STRAFE_MED = 0.8f;

AGP_Camera::AGP_Camera() {

}

void AGP_Camera::Init(glm::vec3 _startPos, glm::vec3 _startLook, glm::ivec2 _screenDim) {

	screenDim = _screenDim;

	//set this as an idenity mat
	vUp = glm::vec3(0.0f, 1.0f, 0.0f);
	vStrafe = glm::vec3(0.0f);
	speed = 0;
	mouseSpeed = 0.4f;
	horAngle = 3.14f;
	vertAngle = 0;
	pos = _startPos;

	cameraMatrix = glm::lookAt(pos, _startLook, vUp);
}

void AGP_Camera::Update(glm::ivec2	_mousePos) {
	
	//till we put dt in main loop just kid along
	static GLfloat dt = 0.06;

	//should probably constraint vertAngle
	this->horAngle -= mouseSpeed * dt * _mousePos.x;
	this->vertAngle -= mouseSpeed * dt * _mousePos.y;	//don't want no inverse

	//restrain vertical look
	if(vertAngle > 1)
		vertAngle = 1;
	else if(vertAngle < -1)
		vertAngle = -1;


	if(vStrafeState.x > 0)	{
		vStrafe = glm::normalize(glm::cross(vUp, vForward));
	}
	else if(vStrafeState.y > 0)	{
		vStrafe = glm::normalize(glm::cross(vForward, vUp));	
	}
	else	{
		vStrafe.x = 0;
		vStrafe.z = 0;
	}	
	vStrafe.x *= PLAYER_STRAFE_MED;
	vStrafe.z *= PLAYER_STRAFE_MED;
	

	//find relavent vectors
	vForward.x = glm::cos(vertAngle) * glm::sin(horAngle);
	vForward.y = glm::sin(vertAngle);
	vForward.z = glm::cos(vertAngle) * glm::cos(horAngle);

	//got two different ways to find the right vector
	//which is best?
	vUp = glm::vec3(0.0f, 1.0f, 0.0f);
	vRight = glm::cross(vForward, vUp);

	/*vRight.x = glm::sin(horAngle - 3.14f/2.0f);
	vRight.y = 0.0f;
	vRight.z = glm::cos(horAngle - 3.14f/2.0f);*/

	glm::normalize(vForward);
	glm::normalize(vRight);

	//vUp = glm::cross( vRight, vForward );

	//create a matrix
	this->cameraMatrix = glm::lookAt(pos, pos+vForward, vUp);

	//update the position if we are moving
	pos+= vForward * dt * speed;
	pos+= vStrafe;
}

bool AGP_Camera::InputUp(SDL_Event const &sdlEvent) {

	if(SDL_BUTTON_LEFT)
	{

	}
	if(SDL_BUTTON_RIGHT)
	{
		speed = 0;
	}

	//keep your friend close!
	switch( sdlEvent.key.keysym.sym)
		{
			case SDLK_ESCAPE:
				return false;
			case SDLK_w:
				vStrafe.y = 0.0f;
				break;
			case SDLK_a:
				//set in here
				//cross prod in update
				vStrafeState.x = 0.0f;
				break;
			case SDLK_s:
				vStrafe.y = 0.0f;

				break;
			case SDLK_d:
				vStrafeState.y = 0.0f;

				break;
			default:
				break;
		}
	return true;
}


bool AGP_Camera::InputDown(SDL_Event const &sdlEvent) {

	if(SDL_BUTTON_RIGHT)
	{
		speed = PLAYER_MOVE_MED;
	}

	//keep your friend close!
	switch( sdlEvent.key.keysym.sym)
		{
			case SDLK_ESCAPE:
				return false;
			case SDLK_w:
				vStrafe.y = PLAYER_STRAFE_MED;

				break;
			case SDLK_a:
				//set in here
				//cross prod in update
				vStrafeState. x = 1.0f;

				break;
			case SDLK_s:
				vStrafe.y = -PLAYER_STRAFE_MED;

				break;
			case SDLK_d:
				vStrafeState.y = 1.0f;

				break;
			default:
				break;
		}
	return true;
}