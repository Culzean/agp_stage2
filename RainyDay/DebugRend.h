#ifndef DEBUG_RENDER_H
#define DEBUG_RENDER_H

#include "stafx.h"

#include "Renderable.h"

class DebugRend : public Renderable {

public:
	DebugRend();
	virtual ~DebugRend();

	virtual void Draw(AGPShader* shader);
	virtual void Init();
	virtual void Update();

	bool loadData(char * fname);
	bool loadData(glm::vec3 data[3]);

	bool loadNorms(glm::vec3* norms, GLint iSize, glm::vec3 _pos);

	void SetNumbVerts( GLuint iVerts )	{	this->iNoVerts = iVerts;	}

	void EndMesh();

private:

};


#endif