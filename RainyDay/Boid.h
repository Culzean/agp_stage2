#ifndef BOID_H
#define BOID_H

#include "stafx.h"
#include "Renderable.h"

class DebugRend;

class Boid : public Renderable {

public:
	Boid();
	virtual ~Boid();

	virtual void Draw(AGPShader* shader);
	virtual void Init();
	virtual void Init(glm::vec3 _pos, bool drawDebug);

	static void DrawDebug(glm::vec3 _vDraw, glm::vec3 _vStart);

	void Boid::Update();

	glm::vec3 getVel()				{	return vVel;	};
	glm::vec3 getPos()				{	return vPos;	};
	glm::vec3 getOrient()			{	return vOrient;	};

	void setVel(glm::vec4 _vel);
	void setPos(glm::vec4 _pos);
	void setOrient(glm::vec4 _ornt);

	/*
	going to use euler angles for the heading of the boid
	so better set the heirachy
	will use xyz parenting strucutre
	so
	so in vec.x is pitch
	vec.y is yaw
	vec.z is roll
	
	*/

	bool InterpolateHeading();

private:

	bool debug;

	glm::vec3 vPos;
	glm::vec3 vVel;
	glm::vec3 vOrient;
	glm::vec3 vAngle;

	GLfloat speed;
	GLfloat preceptionDist;

	glm::vec3 vPrevPos;
	glm::vec3 vPrevVel;

	void BindBuffers();
};

#endif