////////////////////////////////////////////////
//
//Game class included from RT3D project and retro fitted for SDL
//by B00226804

#include "game.h"

#include <SDL.h>
#include <glew.h>

GameClass* GameClass::pGame = 0;

////////////////
//always good to have an error method!
void exitFatalError(char *message) {
	std::cout << message << "  " << std::endl;
	std::cout << SDL_GetError();
	SDL_Quit();
	exit(1);
}

SDL_Window* setupRC(SDL_GLContext &glContext) {

	SDL_Window* window;
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
			exitFatalError("Unable to initialize SDL");

	//need to check what version?
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 0);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	window = SDL_CreateWindow("AGP Swarms!",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if(!window)
		exitFatalError("Unable to create window");

	glContext = SDL_GL_CreateContext(window);
	if(!window)
		exitFatalError("SDL Window Failed!");

	SDL_GL_SetSwapInterval(1);

	return window;
}

void GameClass::Update()
{
	model->player.Update(mousePos);

	int end = model->drawList.size();
	for(int i= 0 ; i < end; ++i )
	{
		model->drawList[i]->Update();
	}	
}

bool GameClass::Init(  )
{
	SDL_Init(SDL_INIT_VIDEO);
	running = true;

	hWindow = setupRC(glContext);

	GLenum err = glewInit();

	if (GLEW_OK != err)
	{	// glewInit failed, something is seriously wrong.
		std::cout << "glewInit failed, aborting." << std::endl;
		exitFatalError("");
		exit (1);
	}

	//init context tools
	projection = glm::perspective(65.0f, 4.0f / 3.0f, 1.0f, 100.0f);
	model = new GameData();
	texManager = TextureManager::GetInstance();
	matrixStack = new AGPMatrixStack();

	MVP = new AGPMatrixStack();
	MVP->LoadIdentity();

	SDL_GetTicks();

	matrixStack->LoadIdentity();
	model->Init();

	SDL_SetRelativeMouseMode(SDL_bool(true));

	return true;
}

void GameClass::Render(SDL_Window* window)
{
	glm::vec3 offset(0.0f, -1.0f, -8.0f);
	glClearColor(0.0, 0.3, 0.8, 1.0);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	matrixStack->push();

	//we want projection * view * model
	//get the order right
	matrixStack->multiMatrix(model->player.getCamera());

	//matrixStack->translate(this->input);

	int end = model->drawList.size();
	for(int i= 0 ; i < end; ++i )
	{
		matrixStack->push();
		
		model->shader.useShader(matrixStack, projection, model->light0, model->drawList[i] );
		model->drawList[i]->Draw(&model->shader);
		matrixStack->pop();
	}	
	//draw some debug lines
	Flock::DrawDebug( glm::vec4(0.0f, 1.0f, 0.0f, 0.0f),glm::vec4(0.0));
	Flock::DrawDebug( glm::vec4(1.0f, 0.0f, 0.0f, 0.0f),glm::vec4(0.0));
	Flock::DrawDebug( glm::vec4(0.0f, 0.0f, 1.0f, 0.0f),glm::vec4(0.0));

	matrixStack->pop();

	SDL_GL_SwapWindow(window);
}

bool GameClass::handleSDLEvent(SDL_Event const &sdlEvent) {
	if(sdlEvent.type == SDL_QUIT)
		return false;
	if(sdlEvent.type == SDL_MOUSEBUTTONUP)
	{	model->player.InputUp(sdlEvent);	}
	if(sdlEvent.type == SDL_MOUSEBUTTONDOWN)
	{	model->player.InputDown(sdlEvent);	}
	if(sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym)
		{
			case SDLK_ESCAPE:
				return false;
			case SDLK_1:
				this->iUserVar = 0;
				break;
			case SDLK_2:
				this->iUserVar = 1;
				break;
			case SDLK_3:
				this->iUserVar = 2;
				break;
			case SDLK_4:
				this->iUserVar = 3;
				break;
			case SDLK_p:
				model->flock0->SetInput(iUserVar, 1);
				return true;
				break;
			case SDLK_l:
				model->flock0->SetInput(iUserVar, -1);
				return true;
				break;
			default:
				break;
		}
		return model->player.InputDown(sdlEvent);
	}
	if(sdlEvent.type == SDL_KEYUP)
	{
		model->player.InputUp(sdlEvent);
	}

	SDL_SetRelativeMouseMode(SDL_bool(true));
	//pol for mouse movement
	SDL_GetRelativeMouseState(&mousePos.x, &mousePos.y);

	SDL_SetRelativeMouseMode(SDL_bool(false));
	//SDL_WarpMouseInWindow(this->hWindow, (SCREEN_WIDTH / 2), (SCREEN_HEIGHT / 2));
	return true;
}

void GameClass::MainLoop(void)
{
	//method contains the main endless loop for rendering

	// Required on Windows... init GLEW to access OpenGL beyond 1.1
	// remove on other platforms
	
	while(running)
	{
		SDL_PollEvent(&sdlEvent);
		running = handleSDLEvent(sdlEvent);

		Update();
		Render(hWindow);
	}
}

void GameClass::CleanUp()
{
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(hWindow);

	delete model;
	delete matrixStack;
	texManager->DestroyInstance();
	SDL_SetRelativeMouseMode(SDL_bool(false));
}