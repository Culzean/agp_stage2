#include "Plane.h"

Plane::Plane(glm::vec3 _pos0, glm::vec3 _pos1, glm::vec3 _pos2, glm::vec3 _pos3)
{
	pos0 = _pos0;
	pos1 = _pos1;
	pos2 = _pos2;
	pos3 = _pos3;

	centre = ( pos2 - pos0 );
	centre /= 2;
	centre += pos0;

	pos3 = centre - pos1;
	pos3 *= 2;
	pos3 += pos1;

	normal = FindNormal();

	//actual width/depth?
	//or projected width/depth?
	glm::vec3 temp0(pos0.x,0.0, pos0.z);
	glm::vec3 temp1(pos1.x,0.0, pos1.z);
	glm::vec3 temp3(pos3.x,0.0, pos3.z);
	width = glm::distance(temp0, temp3);
	depth = glm::distance(temp0, temp1);

	std::cout << "Plane width: " << width << std::endl;
	std::cout << "Plane depth: " << depth << std::endl;

	planeDot = glm::dot(-normal, centre);

	pMesh = new Mesh();
}


GLfloat	Plane::findPoint( GLfloat x, GLfloat z ) {
	//give various configartions of inputs
	//find the missing value such that the point is on the plane

	return ( -1 * ( normal.x * x + normal.z * z + planeDot ) / normal.y );
}

Plane::~Plane(){}

void Plane::Init(GLuint iNoPointsWidth, GLuint iNoPointDepth) {

	
	pMesh->createVertices( getWidth(), getDepth(), iNoPointsWidth, iNoPointDepth  );
}

glm::vec3 Plane::FindNormal()
{
	glm::vec3 vec1 = pos2 - pos1;
	glm::vec3 vec2 = pos0 - pos1;

	glm::vec3 vec3 = pos2 - pos3;
	glm::vec3 vec4 = pos0 - pos3;

	glm::vec3 test1 = ( glm::cross( vec1, vec2 ) );
	glm::vec3 test2 = ( glm::cross( vec4, vec3 ) );

	std::cout << " test norm1 : " << test1[0] << "  " << test1[1] << "  "<<test1[2] << std::endl;
	std::cout << " test norm2 : " << test2[0] << "  " << test2[1] << "  "<<test2[2] << std::endl;

	return glm::normalize( glm::cross( vec1, vec2 ) );
}

void Plane::PrintOut()
{
	std::cout << "Normal x: " << normal[0] << " y: " << normal[1] << " z: " << normal[2]  << std::endl;
}