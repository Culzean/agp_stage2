#include "DebugRend.h"

DebugRend::DebugRend() {

}

DebugRend::~DebugRend() {
	glDeleteBuffers(this->iNoBuffers, vbo);
	glDeleteVertexArrays(1, &vao);
	delete [] vbo;
    delete [] pVerts;
    delete [] pNorms;
}

bool DebugRend::loadData(glm::vec3 data[3]) {

	return 0;
}

void DebugRend::Init() {
	this->iNoBuffers = 2;

	this->vbo = new GLuint[iNoBuffers];
}

void DebugRend::Update() {
//this method shouldn't really be here
}

bool DebugRend::loadNorms(glm::vec3* norms, GLint iSize, glm::vec3 _pos) {

	this->iNoVerts = iSize * 2;

	//load normals for debug display
	pNorms = new glm::vec3[this->iNoVerts];
	pVerts = new glm::vec3[this->iNoVerts];
	for(int i=0; i < this->iNoVerts; i+=2)
	{
		this->pNorms[i] = _pos;
		this->pNorms[i+1] = norms[i];

		this->pVerts[i] = norms[i];
	}

	
	//EndMesh();

	return true;
}

void DebugRend::EndMesh() {

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(this->iNoBuffers, vbo);

	//bind normals buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(GLfloat) * 3, pNorms , GL_DYNAMIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);

	//bind second vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]); // bind 2nd VBO as active buf obj 
	// Copy the colour data from colours to our buffer
	// 15 * sizeof(GLfloat) is the size of the colours array
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(GLfloat) * 3, pNorms, GL_DYNAMIC_DRAW);
	// Colour data is going into attribute index 1 & has 3 floats per vertex 
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);    // Enable attribute index 1 (color)

}

void DebugRend::Draw(AGPShader* shader) {

	//so draw already
	//glBindVertexArray(this->vao);
	glLineWidth(7);
	//update the data to draw
	//glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	/*glBufferData(GL_ARRAY_BUFFER, this->iNoVerts * sizeof(glm::vec3), this->pNorms, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);     // Enable attribute index 0
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);*/

	static float line_vertex[]=
	{
    10,10, 15,-6
	};

	glVertexPointer(this->iNoVerts, GL_FLOAT, 0, this->pNorms);
	glDrawArrays(GL_LINES, 0, iNoVerts );

	glLineWidth(3);

	delete pNorms;
	delete pVerts;

	//shader->onFinish(this->getShaderType());
}