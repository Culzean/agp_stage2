#ifndef OBJECT_H
#define OBJECT_H

#include "stafx.h"

#include "Renderable.h"

class DebugRend;

class Object : public Renderable {

public:
	Object();
	virtual ~Object();

	virtual void Draw(AGPShader* shader);
	virtual void Draw(AGPShader* shader, glm::vec3 input);
	bool loadData(char * fname);
	bool loadData();

	void BeginMesh(GLuint nMaxVerts);
	void EndMesh();
	void AddTriangle(glm::vec3 verts, glm::vec3 norms, glm::vec2 texCoords);

	void AddDebug(AGPShader* shaderManager);

protected:

	GLuint vao;
	GLuint* vbo;
	GLuint iBufferCount;

	GLuint iNumVerts;
	GLuint iNumFaces;


	GLfloat* pVerts;
	GLfloat* pNorms;
	GLfloat* pTexCoords;
	GLuint* pIndexes;

};

#endif