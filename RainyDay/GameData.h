#ifndef GAMEDATA_H
#define GAMEDATA_H

#include "stafx.h"
#include <vector>

class Flock;

class GameData {

public:
	GameData();
	~GameData();

	void Init();
	void CleanUp();

	std::vector<Renderable*> drawList;
	light*				light0;

	AGP_Camera			player;
	AGPShader			shader;
	Flock*				flock0;

private:

	void GameData::loadMaterial( light* light);

	TextureManager*		texManager;
};

#endif