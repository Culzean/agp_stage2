
#include "game.h"
#include "stafx.h"


#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif


int WIDTH = 800, HEIGHT = 600;	//Window width, height!

GameClass *game;

// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char* argv[]) {

	game = GameClass::GetInstance();

	game->Init();

	game->MainLoop();

	game->CleanUp();
	SDL_Quit();

	return 0;
}